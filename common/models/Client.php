<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "Clients".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $email
 * @property string $phone
 * @property int $region_id
 * @property string $organisation
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Regions $region
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Clients';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email', 'phone', 'region_id', 'organisation'], 'required'],
            [['region_id', 'created_at', 'updated_at'], 'integer'],
            [['firstname', 'lastname', 'middlename', 'email', 'phone', 'organisation'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Имя'),
            'lastname' => Yii::t('app', 'Фамилия'),
            'middlename' => Yii::t('app', 'Отчество'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Номер телефона'),
            'region_id' => Yii::t('app', 'Регион'),
            'organisation' => Yii::t('app', 'Организация'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Изменено'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }
}
