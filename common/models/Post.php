<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "Posts".
 *
 * @property int $id
 * @property string $subject
 * @property string $body
 * @property string $photo
 * @property string $subheader
 * @property int $author_id
 * @property int $created_at
 * @property int $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    public $file;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Posts';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'body', 'subheader', 'author_id'], 'required'],
            [['photo'], 'required', 'on' => 'update'],
            [['author_id', 'created_at', 'updated_at'], 'integer'],
            ['body', 'string'],            
            [['subject', 'photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Subject'),
            'body' => Yii::t('app', 'Body'),
            'photo' => Yii::t('app', 'Photo'),
            'subheader' => Yii::t('app', 'Subheader'),
            'author_id' => Yii::t('app', 'Author ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
