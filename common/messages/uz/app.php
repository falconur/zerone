<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/26/18
 * Time: 3:11 PM
 */

return [
    "Главный" => "Asosiy",
    "О нас" => "Biz haqimizda",
    "Продукты" => "Mahsulotlar",
    "Услуги" => "Xizmatlar",
    "Кадровая политика" => "Kadrlar siyosati",
    "Новости" => "Yangiliklar",
    "Контакты" => "Aloqa",
    "Национальный разработчик программных решений в области обеспечения информационной безопасности и контроля действий персонала" => "Axborot xavfsizligi va xodimlarni monitoring qilish sohasida dasturiy maxsulotlani ishlab chiqaruvchi milliy kompaniya",
    "Подробно" => "Batafsil",
    "Наши услуги" => "Bizning xizmatlar",
    "Наш адрес" => "Bizning manzil",
    "г. Ташкент, улица Мукимий, 3" => "Toshkent sh., Muqimiy ko'chasi, 3-uy",
    "Наши продукты" => "Bizning mahsulotlar",
    "Будьте с нами" => "Biz bilan bo'ling",
    "Чтобы узнать о новостях нашего деятельности, подписывайтесь на наш блог" => "Bizning faoliyatimizga oid yangiliklardan xabardor bo'lish uchun obuna bo'ling",
    "Ваш Email" => "Email manzilingiz",
    "Подписываться" => "Obuna bo'lish",
    "Все права защищены" => "Barcha huquqlar himoyalangan",
    "ZERONE - национальный разработчик программных решений в области информационной безопасности и управления персоналом. Мы разрабатываем программы по повышению эффективности коммерческих предприятий и государственных служб, направленные на снижение рисков, связанных с внутренними угрозами информационной безопасности. Продукция компании обеспечивает возможность мониторинга рабочих мест, анализа событий, контроля рабочего времени и производительности труда работников организаций, оповещения об опасных и непродуктивных действиях. Год назад мы начали разрабатывать нашу первую программу DLP, которая называется MSSDLP. Члены нашей команды молоды и имеют опыт работы более 5 лет на своих полях." => "ZERONE - axborot xavfsizligi va kadrlar boshqaruvi sohasida dasturiy maxsulotni ishlab chiqaruvchi milliy kompaniya. Biz savdo korxonalari va davlat xizmatlari samaradorligini oshirish, hamda axborot xavfsizligining ichki tahdidlari bilan bog'liq xatarlarni kamaytirishga yo'naltirilgan dasturlarni ishlab chiqmoqdamiz. Kompaniya mahsuloti ish o'rinlarini kuzatib borish, hodisalarni tahlil qilish, xodimlarning ish vaqti va mehnat unumdorligini nazorat qilish, xavfli va samarasiz faoliyat haqida ogohlantirish kabi xususiyatlarga ega. Bir yil avval, biz o'zimizning birinchi DLP dasturini ishlab chiqdik va MSSDLP deb nomladik. Jamoa a'zolarimiz yosh va o'z sohalarida 5 yildan ortiq tajribaga ega.",
    "Наши клиенты" => "Mijozlar",
    "За короткий срок компании удалось собрать высококвалифицированных специалистов – математиков, программистов, системных аналитиков, менеджеров с опытом работы в ИТ-проектах и в области обеспечения информационной безопасности, в частности в таких ключевых направлениях, как техническая и криптографическая защита информации. Это удалось осуществить благодаря стремлению руководства компании обеспечить своим сотрудникам максимально комфортные условия для раскрытия их профессионального и творческого потенциала. Работы в данном направлении не носят временный характер и схемы мотивации и программы карьерного роста для сотрудников постоянно совершенствуются. Благодаря совершенной и обдуманной кадровой политике в компании сегодня сформирован сплоченный и творческий коллектив с высоким интеллектуалом, который способен решит самые сложные задачи в области обеспечения информационной безопасности." => "",
    "Нашa команда" => "Bizning jamoa",
    "Связывайтесь с нами" => "Biz bilan bog'lanish",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",
    "Главный" => "Asosiy",

];