-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2019 at 12:36 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dlp`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_services`
--

CREATE TABLE `category_services` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_services`
--

INSERT INTO `category_services` (`id`, `name_uz`, `name_ru`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'Axborot xavfsizligini ta\'minlash', 'Обеспечение информационной безопасности', 'fa fa-lock', 1545458517, 1545458517),
(2, 'Dasturiy ta\'minot yaratish', 'Разработка Программного Обеспечения', 'fa fa-laptop', 1545458571, 1545458571),
(3, 'Texnik hujjatlarni tayyorlash', 'Разработка технической документации', 'fa fa-file', 1545458673, 1545458961),
(4, 'Ta\'lim', 'Обучение', 'fa fa-book', 1545458721, 1545458721);

-- --------------------------------------------------------

--
-- Table structure for table `Clients`
--

CREATE TABLE `Clients` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `organisation` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Clients`
--

INSERT INTO `Clients` (`id`, `firstname`, `lastname`, `middlename`, `email`, `phone`, `region_id`, `organisation`, `created_at`, `updated_at`) VALUES
(1, 'Nuriddin', 'Kamardinov', NULL, 'falconur98@gmail.com', '90 5433722', 1, 'TATU', 1544684217, 1544684217),
(2, 'Nuriddin', 'Kamardinov', NULL, 'falconur98@gmail.com', '90 5433722', 1, 'TATU', 1544685546, 1544685546),
(3, 'Nuriddin', 'Kamardinov', NULL, 'falconur98@gmail.com', '90 5433722', 1, 'TATU', 1547722100, 1547722100);

-- --------------------------------------------------------

--
-- Table structure for table `clients_logo`
--

CREATE TABLE `clients_logo` (
  `id` int(11) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients_logo`
--

INSERT INTO `clients_logo` (`id`, `logo`) VALUES
(2, '/logos/20190108090345keyboard_key_shift.png'),
(3, '/logos/20190108090407uzbekistan_gerb_31059.jpg'),
(4, '/logos/20190108090415zangiota-ziyoratgohi.jpg'),
(5, '/logos/20190108090424AdobeStock_93919949-1024x768.jpeg'),
(6, '/logos/20190108090458imageedit_4_5324748985.jpg'),
(7, '/logos/20190108090519image.jpg'),
(8, '/logos/20190108090531images.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1544685175),
('m130524_201442_init', 1544685176);

-- --------------------------------------------------------

--
-- Table structure for table `Pages`
--

CREATE TABLE `Pages` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Posts`
--

CREATE TABLE `Posts` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `subheader` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `author_id` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Posts`
--

INSERT INTO `Posts` (`id`, `subject`, `body`, `subheader`, `photo`, `author_id`, `view`, `created_at`, `updated_at`) VALUES
(3, '7 ШАГ ДЛЯ РАЗРАБОТКИ И РАЗВЕРТЫВАНИЯ СТРАТЕГИИ DLP', '<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Дело не в том, что только предотвращение потери данных должно беспокоить только крупное предприятие, потому что теперь даже небольшие компании всех размеров становятся мишенью для хакеров. Имея DLP, Hackercombat стремится помочь пользователю с этим руководством, чтобы сообщить, что нового в системах предотвращения потери данных, в том числе брокеров безопасности облачного доступа.</span></span></p>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Для каждой компании Data Loss Prevention (DLP) была серьезной проблемой. Ранее основное внимание уделялось защите физических документов, которые могут быть легко похищены преступниками во время транзита.</span></span></p>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Рост Интернета увеличил масштабы, и с этим, увеличил шансы кражи данных. Короче говоря, каналы облегчили работу киберпреступников.</span></span></p>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">DLP известен многолетним обязательством, программа DLP может быть управляемым, прогрессивным процессом, если организации сосредоточены на прогрессивном подходе. По словам Gartner Research, вице-президент Антон Чувакин.</span></span></p>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">В этой таблице приведены общие рекомендации, которым должна следовать ваша стратегия DLP, помните, что при запуске программы предотвращения потери данных необходимо выполнить ряд фундаментальных действий. Это все о выборе правильного решения DLP для вашей организации.</span></span></p>\n\n<ol>\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">Установите ваши данные на приоритет</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Важны не все данные, но очень важно определить приоритетность данных, и то, что вызовет вашу проблему. Компания может захотеть сохранить свою интеллектуальную собственность на первом месте в своих усилиях по DLP, особенно для своих будущих проектов. Ритейлеры и компании, предоставляющие финансовые услуги, могут захотеть сохранить свои данные PCI на высоком приоритете. Медицинские компании хотели бы сохранить свои записи пациентов в безопасности.</span></span></p>\n\n<ol start=\"2\">\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">&nbsp;Классифицировать данные или классифицировать</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Огромная проблема в DLP - классифицировать данные по контексту; связывание с исходным приложением, хранилищем данных или пользователем, создавшим данные. Применение тегов классификации, чтобы позволить организациям отслеживать его использование. Проверка содержимого также полезна и часто поставляется с предварительно настроенными правилами для PCI, PII и других стандартов.</span></span></p>\n\n<ol start=\"3\">\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">Знать, какие данные находятся под угрозой</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Это может быть тип данных, средства шифрования и контроля безопасности могут обеспечивать безопасность, когда данные находятся в состоянии покоя, внутри брандмауэра. Данные, которыми обмениваются партнеры, клиенты и цепочки поставок, риск другой. В этих случаях он часто подвергается наибольшему риску в момент использования на конечных точках. Надежное предотвращение потери данных должно учитывать мобильность данных и моменты, когда данные подвергаются риску.</span></span></p>\n\n<ol start=\"4\">\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">Контролировать все движение данных</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Определите и поймите, как используются данные и их существующее поведение, на основании которого вы можете сказать, насколько это важно. Не все перемещения данных представляют собой потерю данных, но некоторые действия могут увеличить риск потери данных. Организации должны отслеживать все перемещения данных и обеспечивать безопасность своих конфиденциальных данных и определять масштабы проблем, которые должна решать стратегия DLP.</span></span></p>\n\n<ol start=\"5\">\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">Разработайте элементы управления</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Следующим шагом для эффективного предотвращения потери данных является работа с бизнес-менеджерами, чтобы понять и создать элементы управления для снижения риска данных. Мониторинг даст вам представление о том, как данные подвергаются риску. Контроль за использованием данных может быть самым простым из инициативы DLP, в то же время вызывая поддержку у линейных менеджеров. По мере развития программы предотвращения потери данных организации могут разрабатывать более эффективные методы для снижения конкретных рисков.</span></span></p>\n\n<ol start=\"6\">\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">Обучать сотрудников и давать рекомендации</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Одна вещь часто доказывает, что обучение пользователей часто может снизить риск случайной потери данных инсайдерами. Организация может часто обучать сотрудников тому, как определенные действия могут привести к потере данных. Расширенные решения DLP должны побуждать сотрудников к использованию данных, которые могут нарушать политику компании.</span></span></p>\n\n<ol start=\"7\">\n	<li style=\"text-align:justify\"><strong><span style=\"background-color:white\"><span style=\"color:#252324\">Получить контроль</span></span></strong></li>\n</ol>\n\n<p style=\"margin-left:0cm; margin-right:0cm; text-align:justify\"><span style=\"background-color:white\"><span style=\"color:#252324\">Выяснение, какие данные важны, и получение контроля над ними - самый важный первый шаг в предотвращении потери данных, но не последний.&nbsp;Предотвращение потери данных является непрерывным процессом и начинается с целенаправленных усилий. DLP проще в реализации и управлении, и при правильном внедрении он также даст уроки о том, как расширить программу. Со временем вы сможете защитить всю конфиденциальную информацию с минимальным нарушением бизнес-процессов.</span></span></p>\n', 'Дело не в том, что только предотвращение потери данных должно беспокоить только крупное предприятие, потому что теперь даже небольшие компании всех размеров становятся мишенью для хакеров...', '/posts/20190108053121p1.jpg', 1, 6, 1546950681, 1547907395),
(4, '5 причин, по которым вашей организации необходимо DLP', '<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Вы найдете десятки продуктов, называющих себя </span><span style=\"color:#404040\">Data</span> <span style=\"color:#404040\">Loss</span> <span style=\"color:#404040\">Prevention</span><span style=\"color:#404040\"> (</span><span style=\"color:#404040\">DLP</span><span style=\"color:#404040\">). И хотя такая технология может быть частью вашего решения, предотвращение потери данных - это полный и полномасштабный стратегический подход к защите ваших данных. Существует как минимум пять причин, по которым вашей организации необходимо предотвращать потерю данных.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">1. У вас есть все, чтобы потерять</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Данные могут означать разные вещи для разных людей. Это означает одно для финансовых работников, а другое - для менеджеров по операциям, кадровых функций и так далее. Он включает вашу интеллектуальную собственность, записи клиентов, личные данные сотрудников, финансовые показатели и многое, многое другое.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Данные хранят все секреты вашего конкурентного преимущества, инновационных продуктов и будущих планов. Он включает в себя все, что интересует всех ваших заинтересованных сторон. И утечка данных или криминальная кража закрывают компании каждый день - в ожидании их восстановления или смерти.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Опрос 1000 лиц, принимающих решения в бизнесе, проведенный исследовательской компанией </span><span style=\"color:#404040\">Vanson</span> <span style=\"color:#404040\">Bourne</span><span style=\"color:#404040\">, показал:</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">&bull; Каждый четвертый уверен, что их компании пострадают от нарушения безопасности.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">&bull; Стоимость нарушения безопасности в среднем составит почти 1 миллион долларов. Крупные компании могут ожидать еще большую цифру.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">&bull; Три из четырех не верят, что все их бизнес-данные полностью защищены.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">&bull; Двое из пяти считают, что данные на их домашних компьютерах более безопасны, чем на рабочих.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Когда данные означают знания, ваши плотные и глубокие данные важны как никогда. Чем плотнее и глубже знания, тем более надежной должна быть защита от потери данных.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">2. Они умнее тебя</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Кибер-ворам больше нечего делать. Их цель - отрицать, разрушать и разрушать. У них нет другой задачи перед ними. Обладая страстью, временем и финансированием, они вооружены таким образом, что коммерческие и некоммерческие организации не находят себе равных.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Даже предприятия в области </span><span style=\"color:#404040\">DLP</span><span style=\"color:#404040\"> изо всех сил стараются не отставать и перевешивать следующие шаги. Новые вредоносные программы появляются в Интернете весь день, каждый день. Киберпреступники нападают на крупные компании с помощью систем с полной обидой и / или проникают в эти же системы по одному компьютеру или мобильному устройству за раз.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Они нацелены на правительства, избирательные системы и спецслужбы. Такие крупные компании, как </span><span style=\"color:#404040\">Target</span><span style=\"color:#404040\">, </span><span style=\"color:#404040\">Sony</span> <span style=\"color:#404040\">Pictures</span><span style=\"color:#404040\">, </span><span style=\"color:#404040\">Anthem</span> <span style=\"color:#404040\">Healthcare</span><span style=\"color:#404040\"> и </span><span style=\"color:#404040\">Penn</span> <span style=\"color:#404040\">State</span> <span style=\"color:#404040\">University</span><span style=\"color:#404040\">, - это лишь некоторые из мега-организаций, которые пострадали. Они потеряли номера кредитных карт, медицинские записи пациентов, личную переписку и академические записи.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Тем не менее, как пишет </span><span style=\"color:#404040\">Fox</span> <span style=\"color:#404040\">Business</span> <span style=\"color:#404040\">News</span><span style=\"color:#404040\">: &ldquo;Сейчас киберпреступники сталкиваются с киберпреступлениями, когда они занимаются мелким и популярным бизнесом всех мастей: розничными магазинами, развлекательными заведениями, отелями, поликлиниками и даже колледжами. И это подталкивает многих предпринимателей к грани банкротства &rdquo;.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Вы, несомненно, слышали о троянских конях, червях, вирусах и многом другом. Но фишинговая атака, похоже, является последним способом проникновения в ваш бизнес. </span><span style=\"color:#404040\">Spear</span> <span style=\"color:#404040\">phishing</span><span style=\"color:#404040\"> маркирует электронные письма таким образом, чтобы они выглядели так, как если бы они были от известного товара сотруднику, открывшему их. Электронная почта достаточно персонализирована, чтобы подсказывать открытие сообщения и зараженного вложения.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Совсем недавно люди получали электронные письма, сообщающие получателям, что их недавний заказ на </span><span style=\"color:#404040\">Amazon</span><span style=\"color:#404040\"> был отменен. Это похоже на любое электронное письмо </span><span style=\"color:#404040\">Amazon</span><span style=\"color:#404040\">, но после проверки вы заметите, что это &ldquo;</span><span style=\"color:#404040\">http</span><span style=\"color:#404040\">: //&rdquo;, а не &ldquo;</span><span style=\"color:#404040\">https</span><span style=\"color:#404040\">: //&rdquo;, и что отправитель не </span><span style=\"color:#404040\">Amazon</span><span style=\"color:#404040\">. Тем не менее, легко увидеть, сколько клиентов </span><span style=\"color:#404040\">Amazon</span><span style=\"color:#404040\"> могут инстинктивно открыть очевидное сообщение </span><span style=\"color:#404040\">Amazon</span><span style=\"color:#404040\">.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">3. Люди будут совершать ошибки</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Большинство данных исчезают из-за проблем сотрудников, а не из-за внешней преступной деятельности. Некоторые сотрудники намеренно крадут данные, чтобы отомстить за предполагаемое негативное отношение к себе, ради личной выгоды или ни по какой другой причине, кроме как саботировать бизнес.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Сотрудники могут быть серьезным риском для безопасности данных. Например, есть продавец, который использует флэш-накопитель для копирования списка клиентов. Или есть сотрудник отдела кадров, который ворует идентификацию сотрудника. Тогда есть чертежник, который копирует проект. Всякая такая кража означает немедленную или возможную потерю для организации.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Сотрудники используют компьютеры компании, мобильные устройства или другие цифровые соединения для совершения покупок, отправки электронной почты и работы в Интернете. Пользователи находятся в социальных сетях, а также в деловой и личной электронной почте и часто работают в незащищенной среде.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Даже невинные сообщения включают в себя отправку и получение информации, которая может быть загружена плохими вещами, которые распространяются дальше. Одно вредоносное вложение может поразить и разрушить всю вашу систему.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">4. Данные больше, чем когда-либо</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#404040\">Любое нажатие клавиши на любом устройстве в вашей организации создает данные. Он растет в геометрической прогрессии и выпирает в швах. Информационные технологии работают над управлением, управлением и хранением. Но это похоже на управление потоком воды.</span></span></p>\r\n\r\n<p><span style=\"color:#404040\">Среди проблем управления данными - дифференциация качества данных. Данные не отражают их важность, конфиденциальность или приоритет безопасности. Помощь&nbsp;данным в понимании их собственной важности будет иметь большое значение для их передачи и создания.</span></p>\r\n', 'Вы найдете десятки продуктов, называющих себя Data Loss Prevention (DLP). И хотя такая технология может быть частью вашего решения, предотвращение потери данных - это полный и полномасштабный стратегический подход к защите ваших данных...123', '/posts/20190108053121p1.jpg', 1, 32, 1546952123, 1548653806),
(5, 'Что такое предотвращение потери данных (DLP)?', '<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Предотвращение потери данных, или </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\">, представляет собой набор технологий, продуктов и методов, предназначенных для предотвращения выхода конфиденциальной информации из организации.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Данные могут попасть в чужие руки, независимо от того, отправляются ли они по электронной почте или через мгновенные сообщения, через формы веб-сайтов, передачу файлов или другими способами. Стратегии </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> должны включать решения, которые отслеживают, обнаруживают и блокируют несанкционированный поток информации.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Как работает </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\">?</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Технологии </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> используют правила для поиска конфиденциальной информации, которая может быть включена в электронные сообщения, или для обнаружения ненормальной передачи данных. Цель состоит в том, чтобы предотвратить случайную или преднамеренную отправку информации, такой как интеллектуальная собственность, финансовые данные и сведения о сотруднике или клиенте, за пределы корпоративной сети.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Зачем организациям нужны решения </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\">?</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Распространение бизнес-коммуникаций позволило многим людям получить доступ к корпоративным данным. Некоторые из этих пользователей могут быть небрежными или злонамеренными. Результат: множество внутренних угроз, которые могут раскрыть конфиденциальные данные одним щелчком мыши. Многие государственные и отраслевые нормативные акты сделали </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> обязательным требованием.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\">&nbsp;</p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Типы </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> технологий</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> для используемых данных</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Один класс технологий DLP</span><span style=\"color:#4d4c4c\"> защищает используемые данные, определяемые как данные, которые активно обрабатываются приложением или конечной точкой. Эти меры безопасности обычно включают аутентификацию пользователей и контроль их доступа к ресурсам.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> для данных в движении</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">Когда конфиденциальные данные передаются по сети, необходимы технологии </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\">, чтобы гарантировать, что они не будут направлены за пределы организации или в незащищенные области хранения. Шифрование играет большую роль на этом этапе. Безопасность электронной почты также имеет решающее значение, так как через этот канал проходит много деловых коммуникаций.</span></span></p>\r\n\r\n<p style=\"margin-left:0cm; margin-right:0cm\"><span style=\"background-color:white\"><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> для данных в состоянии покоя</span></span></p>\r\n\r\n<p><span style=\"color:#4d4c4c\">Даже данные, которые не перемещаются или не используются, требуют гарантий. Технологии </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> защищают данные, находящиеся в различных средах хранения, в том числе в облаке. </span><span style=\"color:#4d4c4c\">DLP</span><span style=\"color:#4d4c4c\"> может размещать элементы управления, чтобы обеспечить доступ к данным только авторизованным пользователям и отслеживать их доступ в случае утечки или кражи.</span></p>\r\n', 'Предотвращение потери данных, или DLP, представляет собой набор технологий, продуктов и методов, предназначенных для предотвращения выхода конфиденциальной информации из организации...', '/posts/20190108053121p1.jpg', 1, 7, 1546952226, 1548832058);

-- --------------------------------------------------------

--
-- Table structure for table `Products`
--

CREATE TABLE `Products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description_uz` text NOT NULL,
  `description_ru` text NOT NULL,
  `subheader_uz` varchar(255) NOT NULL,
  `subheader_ru` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Products`
--

INSERT INTO `Products` (`id`, `name`, `description_uz`, `description_ru`, `subheader_uz`, `subheader_ru`, `icon`, `created_at`, `updated_at`) VALUES
(4, 'DLP', 'Great product for security', 'Great product for security', 'Great product for security', 'Great product for security', '/products/20181218104313AdobeStock_93919949-1024x768.jpeg', 1545129793, 1545129793),
(5, 'DLP', '<p>sdfgsfd</p>\r\n', '<p>gfdsgsdfgs</p>\r\n', '', '', '/products/20190116062318keyboard_key_shift.png', 1547619798, 1547619798);

-- --------------------------------------------------------

--
-- Table structure for table `Regions`
--

CREATE TABLE `Regions` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Regions`
--

INSERT INTO `Regions` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'Andijon', 'Андижан', 123, 123);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name_uz`, `name_ru`, `category_id`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'no', 'Обследование объектов информатизации и разработка политики информационной безопасности', 1, '', 1545459244, 1545459244),
(2, 'no', 'Создание системы обеспечения информационной безопасности, соответствующей политике информационной безопасности', 1, '', 1545459264, 1545459264),
(3, 'no', 'Консалтинг по вопросам разработки комплексных систем защиты информации для объектов информатизации различного класса', 1, '', 1545459284, 1545459284),
(4, 'no', 'Проектирование защищённых информационных систем', 1, '', 1545459297, 1545459297),
(5, 'no', 'Подготовка объектов информатизации к аттестаци', 1, '', 1545459311, 1545459311),
(6, 'no', 'Создание и организация работы ЦРК ЭЦП', 1, '', 1545459333, 1545459333),
(7, 'no', 'Организация создания и обеспечения безопасности защищённых виртуальных частных сетей (VPN)', 1, '', 1545459351, 1545459351),
(8, 'no', 'Разработка средств и систем криптографической и технической защиты', 2, '', 1545459380, 1545459384),
(9, 'no', 'Разработка информационных систем', 2, '', 1545459404, 1545459404),
(10, 'no', 'Разработка десктопных приложений для Windows/Linux', 2, '', 1545459420, 1545459420),
(11, 'no', 'Разработка серверных приложений', 2, '', 1545459434, 1545459434),
(12, 'no', 'Разработка веб сайтов и информационных порталов', 2, '', 1545459450, 1545459549),
(13, 'no', 'Разработка мобильных приложений', 2, '', 1545459499, 1545459503),
(14, 'no', 'Концепции и Стратегии развития ИКТ в организациях', 3, '', 1545459570, 1545459570),
(15, 'no', 'ПТЭР, ТЭО, ТЭР', 3, '', 1545459587, 1545459587),
(16, 'no', 'Технического задания', 3, '', 1545459600, 1545459600),
(17, 'no', 'Политики информационной безопасности', 3, '', 1545459611, 1545459611),
(18, 'no', 'Эксплуатационно-технической документации', 3, '', 1545459625, 1545459625),
(19, 'no', 'Повышение квалификации специалистов по общим вопросам обеспечения', 4, '', 1545459647, 1545459647),
(20, 'no', 'курсы по интеграции средств криптографической и технической защиты информации в прикладные приложения, в том числе в системы электронного документооборота', 4, '', 1545459658, 1545459658),
(21, 'no', 'Курсы по организации инфраструктуры открытых ключей (PKI)', 4, '', 1545459677, 1545459677),
(22, 'no', 'Cпециализированные курсы по программированию', 4, '', 1545459698, 1545459698);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'user', 'Gozx_vCIagUNq6ftYOdNbZ1JquRvjT05', '$2y$13$o9P1Q./Z.lnhohul.nQXUeKnu3udaL9m02QTIHiNhOkGptcyJs0IC', NULL, 'nuriddin@boostin.uz', 10, 1544685180, 1544685180);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_services`
--
ALTER TABLE `category_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Clients`
--
ALTER TABLE `Clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `region_id` (`region_id`);

--
-- Indexes for table `clients_logo`
--
ALTER TABLE `clients_logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `Pages`
--
ALTER TABLE `Pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Posts`
--
ALTER TABLE `Posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Products`
--
ALTER TABLE `Products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Regions`
--
ALTER TABLE `Regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_services`
--
ALTER TABLE `category_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Clients`
--
ALTER TABLE `Clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `clients_logo`
--
ALTER TABLE `clients_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Pages`
--
ALTER TABLE `Pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Posts`
--
ALTER TABLE `Posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Products`
--
ALTER TABLE `Products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Regions`
--
ALTER TABLE `Regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Clients`
--
ALTER TABLE `Clients`
  ADD CONSTRAINT `fk_clients_region` FOREIGN KEY (`region_id`) REFERENCES `Regions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;