<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        'temp/font-awesome/css/font-awesome.min.css',
        'temp/css/owl.carousel.css',
        'temp/css/owl.theme.css',
        'temp/css/owl.transitions.css',
        'temp/css/animate.css',
        'temp/css/lightbox.css',
        'temp/css/style.css',
        'temp/css/responsive.css',               
    ];
    public $js = [
        // 'temp/js/count-to.js',
        // 'temp/js/google-map-init.js',
        // 'temp/js/jquery-2.1.3.min.js',
        // 'temp/js/jquery-migrate-1.2.1.min.js',
        // 'temp/js/jquery.appear.js',
        // 'temp/js/jquery.fitvids.js',
        // 'temp/js/jquery.isotope.js',
        // 'temp/js/jquery.nicescroll.js',
        // 'temp/js/jquery.nicescroll.min.js',
        // 'temp/js/lightbox.js',
        // 'temp/js/map.js',
//        'temp/js/modernizrr.js',
//        'temp/js/owl.carousel.js',
//        'temp/js/owl.carousel.min.js',
        // 'temp/js/script.js',
        // 'temp/js/slick.min.js',
//        'temp/js/styleswitcher.js'
    ];
    public $depends = [
//        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
