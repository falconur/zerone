<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-view">
        <!-- Start Header Section -->
        <div class="page-header">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1><?= $model->name ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Section -->

        <!-- Start Blog Page Section -->
        <div class="container">
            <div class="row">

                <div class="col-md-9">  
                    <div class="">
                    <h1 class="post-title"><?= $model->subheader_ru ?></h1>                                                
                        <p class="post-content"><?= $model->description_ru ?></p>                        
                    </div>                                  
                </div>               
                <div class="col-md-3">                    
                    <div class="product-image">
                        <img src="<?= $model->icon; ?>" class="img-responsive" alt="Blog image">   
                    </div>                    
                </div>                                         
            </div>
        </div>
        <!-- End Blog Page Section -->
</div>
