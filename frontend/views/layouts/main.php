<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use frontend\services\Collections;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Yii::$app->name ?></title>
    <?php $this->head() ?>
    <script src="/temp/js/modernizrr.js"></script>
</head>
<body>
<?php $this->beginBody() ?>   
    
  <header class="clearfix">
  
      <!-- Start Top Bar -->
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="top-bar">
                      <div class="row">
                              
                          <div class="col-md-6">
                              <!-- Start Contact Info -->
                              <ul class="contact-details">
                                  <li><a href="#"><i class="fa fa-phone"></i> +998 (90) 129-9977</a>
                                  </li>
                                  <li><a href="#"><i class="fa fa-envelope-o"></i> info@zerone.uz</a>
                                  </li> 
                              </ul>
                              <!-- End Contact Info -->
                          </div><!-- .col-md-6 -->
                          
                          <div class="col-md-6">
                              <!-- Start Social Links -->
                              <ul class="social-list">
                                  <li>
                                      <a href="https://facebook.com/zerone.uz"><i class="fa fa-facebook"></i></a>
                                  </li>
                                  <li>
                                      <a href="http://twitter.com/zerone_uz"><i class="fa fa-twitter"></i></a>
                                  </li>
                                  <!-- <li>
                                      <a href="#"><i class="fa fa-linkedin"></i></a>
                                  </li>                                   -->
                              </ul>
                              <!-- End Social Links -->
                          </div><!-- .col-md-6 -->
                      </div>
                          
                          
                  </div>
              </div>                        

          </div><!-- .row -->
      </div><!-- .container -->
      <!-- End Top Bar -->
  
      <!-- Start  Logo & Naviagtion  -->
      <div class="navbar navbar-default navbar-top">
          <div class="container">
              <div class="navbar-header">
                  <!-- Stat Toggle Nav Link For Mobiles -->
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <i class="fa fa-bars"></i>
                  </button>
                  <!-- End Toggle Nav Link For Mobiles -->                  
                    <a class="navbar-brand" href="/"><img class="logo" src="/temp/images/logo.png" > </a>
              </div>
              <div class="navbar-collapse collapse">                  
                  <!-- Start Navigation List -->                  
                  <ul class="nav navbar-nav navbar-right">
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'index') echo "class='active'" ?>  
                            href="<?= Url::to(['/']) ?>"><?= Yii::t('app', 'Главный') ?></a>
                      </li>
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'about') echo "class='active'" ?>
                            href="<?= Url::to(['/about']) ?>"><?= Yii::t('app', 'О нас') ?></a>
                      </li>
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'portfolio') echo "class='active'" ?>
                            href="<?= Url::to(['/portfolio']) ?>"><?= Yii::t('app', 'Продукты') ?></a>
                      </li>
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'service') echo "class='active'" ?>
                            href="<?= Url::to(['/service']) ?>"><?= Yii::t('app', 'Услуги') ?></a>
                      </li>                      
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'team') echo "class='active'" ?>
                            href="<?= Url::to(['/team']) ?>"><?= Yii::t('app', 'Кадровая политика') ?></a>                          
                      </li>
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'post') echo "class='active'" ?>
                            href="<?= Url::to(['post/index']) ?>"><?= Yii::t('app', 'Новости') ?></a>
                      </li>
                      <li>
                          <a <?php if (Yii::$app->controller->id === 'site' && Yii::$app->controller->action->id === 'contact') echo "class='active'" ?> 
                            href="<?= Url::to(['/contact']) ?>"><?= Yii::t('app', 'Контакты') ?></a>
                      </li>
                  </ul>                  
                  <!-- End Navigation List -->
              </div>
          </div>
      </div>
      <!-- End Header Logo & Naviagtion -->
      
  </header>     
  
  
  <?= $content ?>

  <!-- Start Footer Section -->
  <section id="footer-section" class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="section-heading-2">
                        <h3 class="section-title">
                            <span><?= Yii::t('app', 'Наш адрес') ?></span>
                        </h3>
                    </div>
                    
                    <div class="footer-address">
                        <ul>
                            <li class="footer-contact"><i class="fa fa-home"></i><?= Yii::t('app', "г. Ташкент, улица Мукимий, 3"); ?></li>
                            <li class="footer-contact"><i class="fa fa-envelope"></i><a href="#">info@zerone.uz</a></li>
                            <li class="footer-contact"><i class="fa fa-phone"></i>+998 (90) 129-9977</li>
                            <li class="footer-contact"><i class="fa fa-globe"></i><a href="#" target="_blank">www.zerone.uz</a></li>
                        </ul>
                    </div>
                </div><!--/.col-md-4 -->

                <div class="col-md-4">
                    <div class="section-heading-2">
                        <h3 class="section-title">
                            <span><?= Yii::t('app', 'Наши продукты') ?></span>
                        </h3>
                    </div>
                    
                    <div class="flickr-widget">
                        <ul class="flickr-list">
                            <?php foreach (Collections::getProducts() as $value): ?>
                                <li>
                                    <a href="<?= Url::to(['product/view', 'id' => $value->id]) ?>" data-lightbox="picture-1">
                                        <img src="<?= $value->icon; ?>" alt="" class="img-responsive">
                                    </a>
                                </li>
                            <?php endforeach; ?>                            
                        </ul>
                    </div>
                </div><!--/.col-md-4 -->
                                
                <div class="col-md-4">
                    <div class="section-heading-2">
                        <h3 class="section-title">
                            <span><?= Yii::t('app', 'Будьте с нами') ?></span>
                        </h3>
                    </div>
                    <div class="subscription">
                        <p><?= Yii::t('app', 'Чтобы узнать о новостях нашего деятельности, подписывайтесь на наш блог') ?></p>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="<?= Yii::t('app', "Ваш Email") ?>" id="name" required data-validation-required-message="Please enter your name.">
                            <input type="submit" class="btn btn-primary" value="<?= Yii::t('app', "Подписываться") ?>">
                        </div>
                    </div>
                </div>                
            </div><!--/.row -->
        </div><!-- /.container -->
    </section>
    <!-- End Footer Section -->
  
    
    <!-- Start CCopyright Section -->
    <div id="copyright-section" class="copyright-section">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="copyright">
                        © <?= date('Y') ?> <a href="http://www.zerone.uz">ZERONE</a>. <?= Yii::t('app', 'Все права защищены'); ?>
                    </div>
                </div>                                
            </div><!--/.row -->
        </div><!-- /.container -->
    </div>
    <!-- End CCopyright Section -->
    
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '6mqNEBxLyc';var d=document;var w=window;function l(){var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
<!-- {/literal} END JIVOSITE CODE -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
