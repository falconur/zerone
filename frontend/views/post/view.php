<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\helpers\Post;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">
    <!-- Start Header Section -->
    <div class="page-header">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?= Yii::t('app', "Новости"); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Section -->

    <div class="container">
        <div class="row">            
            <!-- Start Blog Body Section -->
            <div class="col-md-8 blog-body">                
                <!-- Start Blog post -->
                <div class="single-blog-post">
                    <div class="post-img">
                        <img src="<?= $model->photo ?>" class="img-responsive" alt="Blog image">
                    </div>
                    <h1 class="post-title"><?= $model->subject ?></h1>                    
                    <div class="post-meta">
                        <ul class=" pull-left">
                            <li><i class="fa fa-clock-o"></i><?= Yii::$app->formatter->asDateTime($model->created_at, 'short') ?></li>
                        </ul>
                    </div>
                    <p class="post-content"><?= $model->body ?></p>                    
                    <div class="item-content-footer">
                        <ul>
                            <li><?= Yii::t('app', "Количество просмотров") . ": " . $model->view ?></li>                           
                        </ul>
                    </div>
                    
                </div>
                <!-- End Blog Post -->                                
            </div>
            <!-- End Blog Body Section -->
            
            <!-- Start Sidebar Section -->
            <div class="col-md-4 sidebar right-sidebar">
                                           
                <!-- Start Recent Post Widget -->
                <div class="widget widget-recent-post">
                    
                    <div class="section-heading-2">
                        <h3>
                            <span><?= Yii::t('app', "Популярные посты"); ?></span>
                        </h3>
                    </div>
                    <?php foreach (Post::byView() as $key => $value): ?>
                        <div class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" src="<?= $value->photo ?>" alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="<?= Url::to(['post/view', 'id' => $value->id]) ?>"><?= $value->subject ?></a></h4>
                                <ul>                                        
                                    <li><?= Yii::$app->formatter->asDateTime($value->created_at, 'short') ?></li>
                                </ul>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- End Recent Post Widget -->                             
            </div>
            <!-- End Sidebar Section -->            
        </div>
    </div>
    <!-- End Blog Page Section -->

</div>
