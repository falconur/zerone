<?php

use frontend\helpers\Post;
use yii\helpers\Url;

?>
<!-- Start Header Section -->
<div class="page-header">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?= Yii::t('app', "Новости"); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- End Header Section -->
        
        <!-- Start Blog Page Section -->
        <div class="container">
            <div class="row">
                
                <!-- Start Blog Body Section -->
                <div class="col-md-8 blog-body">
                    <?php foreach (Post::mainPage() as $key => $value): ?>
                        <!-- Start Blog post -->
                        <div class="blog-post">
                            <div class="post-img">
                                <img src="<?= $value->photo ?>" class="img-responsive" alt="Blog image">
                            </div>
                            <h1 class="post-title"><a href="<?= Url::to(['post/view', 'id' => $value->id]) ?>"><?= $value->subject ?></a></h1>
                            
                            <ul class="post-meta">
                                <li><i class="fa fa-clock-o"></i><?= Yii::$app->formatter->asDateTime($value->created_at, 'short') ?></li>                                
                            </ul>
                            
                            <p class="post-content"><?= $value->subheader ?></p>
                            <a href="<?= Url::to(['post/view', 'id' => $value->id]) ?>" class="btn btn-primary readmore"><?= Yii::t('app', "Подробно"); ?></a>
                        </div>
                        <!-- End Blog Post -->
                    <?php endforeach; ?>                     
                    <!-- Start Pagination -->
                    <!-- <nav>
                        <ul class="pagination">
                            <li class="disabled"><a href="#" aria-label="Start">Start</a></li>
                            <li class="disabled"><a href="#" aria-label="Previous">Prev</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">Next</a></li>
                            <li><a href="#">End</a></li>
                        </ul>
                    </nav> -->
                    <!-- End Pagination -->
                    
                </div>
                <!-- End Blog Body Section -->
                
                <!-- Start Sidebar Section -->
                <div class="col-md-4 sidebar right-sidebar">
                                        
                    <!-- Start Recent Post Widget -->
                    <div class="widget widget-recent-post">
                        
                        <div class="section-heading-2">
                            <h3>
                                <span><?= Yii::t('app', "Популярные посты"); ?></span>
                            </h3>
                        </div>
                        <?php foreach (Post::byView() as $key => $value): ?>
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="<?= $value->photo ?>" alt="...">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="<?= Url::to(['post/view', 'id' => $value->id]) ?>"><?= $value->subject ?></a></h4>
                                    <ul>                                        
                                        <li><?= Yii::$app->formatter->asDateTime($value->created_at, 'short') ?></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach; ?>                      
                        
                    </div>
                    <!-- End Recent Post Widget -->                    
                </div>
                <!-- End Sidebar Section -->
                
            </div>
        </div>
        <!-- End Blog Page Section -->