<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Region;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'firstname')->textInput(['maxlength' => true, 'placeholder' => 'Имя'])->label(false) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true, 'placeholder' => 'Фамилия'])->label(false) ?>
        </div>        
    </div>    

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email'])->label(false) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Номер телефона'])->label(false) ?>
        </div>        
    </div>    

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'name_'. Yii::$app->language), ['placeholder' => 'Выберите регион'])->label(false) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'organisation')->textInput(['maxlength' => true, 'placeholder' => 'Организация'])->label(false) ?>
        </div>        
    </div>    
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
