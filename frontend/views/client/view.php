<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = "Дата";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container">
    <div class="client-view">

        <h1 style="margin: 20px"><?= Yii::t('app', 'Текущая дата оправлена. Ждите, скоро к вашей почте отправим код') ?></h1>        

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [                
                'firstname',
                'lastname',                
                'email:email',
                'phone',
                'region_id',
                'organisation',                
            ],
        ]) ?>

    </div>
</div>