<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">

    <div class="alert alert-success" style="margin: 30px;">
        <?= nl2br(Html::encode($message)) ?>
    </div>

</div>
