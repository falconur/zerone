<?php

use frontend\services\Collections;

?>

<div class="page-header">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?= Yii::t('app', 'Наши сервисы ')?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Header Section -->
                
        <!-- Start Service Section -->
        <section id="service-section">
            <div class="container">                                
                <?php foreach (Collections::getServices() as $key => $value): ?>
                    <div id="<?= $key ?>" class="row">
                        <div class="col-md-2">
                            <div class="services-post" id="page">
                                <a href="#<?= $key ?>"><i class="<?= $value->icon ?>"></i></a>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h2 id="service-head"><?= $value->name_ru ?></h2>
                            <ul>
                                <?php foreach ($value->services as $key1 => $value1): ?>
                                    <li><p><?= $value1->name_ru ?></p> </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <ul>                                
                                <?php foreach ($value->services as $key1 => $value1): ?>
                                    <li><p><?= $value1->name_ru ?></p> </li>
                                <?php endforeach; ?>
                            </ul>                                
                        </div>
                    </div>                         -->
                <?php endforeach; ?>                
            </div>
        </section>
        <!-- Start Service Section -->
        
        
        <!-- Start Fun Facts Section -->
    <!-- <section class="fun-facts">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-cloud-upload"></i>
                        <div class="timer" id="item1" data-to="991" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Files uploaded</h3>                               
                      </div>
                    </div>  
                    <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInUp" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-check"></i>
                        <div class="timer" id="item2" data-to="7394" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Projects completed</h3>                               
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInDown" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-code"></i>
                        <div class="timer" id="item3" data-to="18745" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Lines of code written</h3>                               
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInRight" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-male"></i>
                        <div class="timer" id="item4" data-to="8423" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Happy clients</h3>                               
                      </div>
                    </div>
            </div>
        </div>
    </section> -->
    <!-- End Fun Facts Section -->        