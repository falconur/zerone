<?php

?>

<div class="page-header">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?= Yii::t('app', 'Нашa команда')?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Header Section -->
 <!-- Start Team Member Section -->
<section id="team-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="team-text">
                    <p><?= Yii::t('app', "За короткий срок компании удалось собрать высококвалифицированных специалистов – математиков, программистов, системных аналитиков, менеджеров с опытом работы в ИТ-проектах и в области обеспечения информационной безопасности, в частности в таких ключевых направлениях, как техническая и криптографическая защита информации. Это удалось осуществить благодаря стремлению руководства компании обеспечить своим сотрудникам максимально комфортные условия для раскрытия их профессионального и творческого потенциала. Работы в данном направлении не носят временный характер и схемы мотивации и программы карьерного роста для сотрудников постоянно совершенствуются. Благодаря совершенной и обдуманной кадровой политике в компании сегодня сформирован сплоченный и творческий коллектив с высоким интеллектуалом, который способен решит самые сложные задачи в области обеспечения информационной безопасности.") ?></p>                         
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                <div class="team-member">
                    <img src="/temp/images/team/Umid.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Мамажонов Умид</h4>
                        <p>Инженер-программист</p>
                        <!-- <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                <div class="team-member">
                    <img src="/temp/images/team/Azam.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Набижонов Азам</h4>
                        <p>Инженер-программист</p>
                        <ul>
                            <li><a href="http://facebook.com/azamjon.nabijonov"><i class="fa fa-facebook"></i></a></li>                            
                            <!-- <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                                                         -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="900ms">
                <div class="team-member">
                    <img src="/temp/images/team/Ulugbek.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Улугбек Самигжонов</h4>
                        <p>Инженер-программист</p>
                        <ul>                            
                            <li><a href="http://linkedin.com/in/ulugbek-samigjonov"><i class="fa fa-linkedin"></i></a></li>                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1200ms">
                <div class="team-member">
                    <img src="/temp/images/team/Mirsaid.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Мирсаид Равилов</h4>
                        <p>Инженер-программист</p>
                        <ul>
                            <!-- <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Team Member Section -->