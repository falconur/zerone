<?php

use yii\helpers\Url;
use frontend\services\Collections;


?>

 <div class="page-header">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1><?= Yii::t('app', 'Наши продукты') ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Section -->       
        
        
        <!-- Start Portfolio Section -->
        <section id="portfolio" class="portfolio-section-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <!-- Start Portfolio items -->
                        <ul id="portfolio-list">
                            <?php foreach (Collections::getProducts() as $key => $value): ?> 
                                <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                                    <h3 class="" ><?= $value->name ?></h3>
                                    <a href="<?= Url::to(['product/view', 'id' => $value->id]) ?>">
                                        <div class="portfolio-item">
                                            <img src="<?= $value->icon; ?>" class="img-responsive" alt="" />
                                            <div class="portfolio-caption">
                                                <h4><?= $value->subheader_ru ?></h4>                                                                                        
                                            </div>
                                        </div>
                                    </a>
                                </li>                            
                            <?php endforeach; ?>
                        </ul>
                        <!-- End Portfolio items -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End Portfolio Section -->
        