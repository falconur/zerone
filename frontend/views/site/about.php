<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\services\Collections;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

  <div class="page-header">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1><?= Yii::t('app', 'О нас') ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Section -->
        
        
        <!-- Start About Us Section -->
    <section id="about-section" class="about-section">
        <div class="container">
            <div class="row">
               <div class="col-md-5">
                   <div class="about-img">
                       <img src="/temp/images/corporate1.jpg" class="img-responsive" alt="About images">
                       <div class="head-text">                          
                       </div>
                   </div>
               </div>
               <div class="col-md-7">
                   <div class="about-text">
                       <p><?= Yii::t('app', "ZERONE - национальный разработчик программных решений в области информационной безопасности и управления персоналом. Мы разрабатываем программы по повышению эффективности коммерческих предприятий и государственных служб, направленные на снижение рисков, связанных с внутренними угрозами информационной безопасности. Продукция компании обеспечивает возможность мониторинга рабочих мест, анализа событий, контроля рабочего времени и производительности труда работников организаций, оповещения об опасных и непродуктивных действиях. Год назад мы начали разрабатывать нашу первую программу DLP, которая называется MSSDLP. Члены нашей команды молоды и имеют опыт работы более 5 лет на своих полях.") ?></p>
                   </div>                  
                   
               </div>                
            </div>
        </div>
    </section>
        
     
    <!-- Start Client Section -->
    <?php if(count(Collections::getClientsLogos()) > 0): ?>
        <div id="client-section" style="border-top: 1px solid #f1f1f1;">
            <div class="container">
                <div class="client-box">
                    <h2><?= Yii::t('app', "Наши клиенты"); ?></h2>
                    <section class="customer-logos slider">
                        <?php foreach (Collections::getClientsLogos() as $key => $value): ?> 
                            <div class="slide"><img src="<?= $value->logo ?>"></div>
                        <?php endforeach; ?>
                    </section>
                </div>                
            </div>
        </div>   
    <?php endif; ?>
    <?php $this->registerJsFile('https://code.jquery.com/jquery-2.2.4.min.js', ['position' => \yii\web\View::POS_HEAD]) ?>
    <?php $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js', ['position' => \yii\web\View::POS_HEAD]) ?>
    <?php $this->registerJsFile('/slider/js/slider.js') ?>
    <?php $this->registerCssFile('/slider/css/main.css') ?>
    <!-- End Client Section -->
    
        
        