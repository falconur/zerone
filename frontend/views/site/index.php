<?php

use frontend\services\Collections;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
 <!-- Start Header Section -->
<div class="banner">  
    <div class="overlay">
        <div class="container">
            <div class="intro-text">
                <h1>ZERONE<span></span></h1>
                <p><?= Yii::t('app', "Национальный разработчик программных решений в области обеспечения информационной безопасности и контроля действий персонала") ?></p>
                <a href="<?= Url::to(['/about']) ?>" class="page-scroll btn btn-primary"><?= Yii::t('app', 'Подробно') ?></a>
            </div>
        </div>            
    </div>
</div>
<!-- End Header Section -->
        
        
    <!-- Start Portfolio Section -->
    <section id="portfolio" class="portfolio-section-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2><?= Yii::t('app', "Новости"); ?></h2>                        
                    </div>                        
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">                    
                    <!-- Start Portfolio items -->
                    <ul id="portfolio-list">
                        <?php foreach (Collections::getPosts() as $value): ?>                                                    
                            <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">                                
                                <a href="<?= Url::to(['post/view', 'id' => $value->id]) ?>" >                                
                                    <h3><?= $value->subject ?></h3>                                        
                                </a>
                                <div class="post-item">                                    
                                    <img src="<?= $value->photo; ?>" class="img-responsive" alt="" />                                        
                                    <div class="portfolio-caption">                                                                            
                                        <h4><?= Yii::t('app', "Подробно"); ?></h4>                                            
                                    </div>                                    
                                </div>                                
                            </li>                            
                        <?php endforeach; ?>                        
                    </ul>
                    <!-- End Portfolio items -->
                    <div class="btnnew">
                        <?= \yii\helpers\Html::a("Все новости", Url::to('post/index'), ['class' => 'btn btn-primary newsbtn']) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Portfolio Section -->
        
    <!-- Start Service Section -->
    <section id="service-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2><?= Yii::t('app', "Наши услуги"); ?></h2>                        
                    </div>                        
                </div>
            </div>
            <div class="row">                
                <?php foreach (Collections::getServices() as $key => $value): ?>                    
                    <div class="col-md-3">
                        <div class="services-post" id="index">
                            <a href="<?= Url::to(['/service', '#' => $key]) ?>"><i class="<?= $value->icon ?>"></i></a>
                            <?php if (Yii::$app->language == "ru"): ?>
                                <h2><?= $value->name_ru ?></h2>                            
                            <?php else: ?>
                                <h2><?= $value->name_uz ?></h2>                            
                            <?php endif; ?>
                        </div>
                    </div>                
                <?php endforeach; ?>                
            </div>
        </div>
    </section>
    <!-- Start Service Section -->
        
    <!-- Start Client Section -->
    <?php if(count(Collections::getClientsLogos()) > 0): ?>
    <!-- <div id="client-section" style="border-top: 1px solid #f1f1f1;">
        <div class="container">
            <div class="client-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                            <h2><?= Yii::t('app', "Наши клиенты"); ?></h2>                        
                        </div>                        
                    </div>
                </div>
                <section class="customer-logos slider">
                    <?php foreach (Collections::getClientsLogos() as $key => $value): ?> 
                        <div class="slide"><img src="<?= $value->logo ?>"></div>
                    <?php endforeach; ?>
                </section>
            </div>                
        </div>
    </div>    -->
    

    <?php $this->registerJsFile('https://code.jquery.com/jquery-2.2.4.min.js', ['position' => \yii\web\View::POS_HEAD]) ?>
    
    <?php endif; ?>

    <!-- End Client Section -->    
