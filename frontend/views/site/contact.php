<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">   
  <!-- Start Header Section -->
  <div class="page-header">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1><?= Yii::t('app', 'Связывайтесь с нами') ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Section -->    

        <!-- Start Contact Us Section -->
    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">   
                    <div class="about-list">
                       <h4><?= Yii::t('app', "Наш адрес"); ?></h4>
                       <ul>
                            <li><i class="fa fa-home"></i><?= Yii::t('app', "г. Ташкент, улица Мукимий, 3"); ?></li>
                            <li><i class="fa fa-phone"></i>+998 (90) 129-9977</li>
                            <li><i class="fa fa-envelope"></i><a href="#">info@zerone.uz</a></li>                            
                       </ul>
                       
                       <!-- <h4><?= Yii::t('app', "Страницы на соц. сетях"); ?></h4>
                       <ul>
                           <li><i class="fa fa-facebook"></i>Facebook</li>
                           <li><i class="fa fa-twitter"></i>Twitter</li>
                           <li><i class="fa fa-chrome"></i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</li>
                           <li><i class="fa fa-check-square"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                       </ul> -->
                   </div>               
                </div>
                <div class="col-lg-8">                  
                    <div style="height: 400px" id="mapid"></div>
                </div>
            </div>            
        </div>
        
    </section>

    
    
    <?php $this->registerCssFile('https://unpkg.com/leaflet@1.4.0/dist/leaflet.css', [
        'integrity' => "sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==",
        'crossorigin' => "", 
        // 'position' => \yii\web\View::POS_BEGIN
    ]) ?>
    <?php $this->registerJsFile('https://unpkg.com/leaflet@1.4.0/dist/leaflet.js', [
        'integrity' => "sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==",
        'crossorigin' => "", 
        'position' => \yii\web\View::POS_HEAD
    ]) ?>

    <?php $this->registerJsFile('/js/map.js  ') ; ?>
   
