<?php

namespace frontend\services;

use common\models\Product;
use common\models\CategoryServices;
use common\models\Post;
use common\models\ClientsLogo;

class Collections
{
    public static function getProducts()
    {
        return Product::find()->all();
    }

    public static function getServices()
    {
        return CategoryServices::find()->all();
    }

    public static function getPosts()
    {
        return Post::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(3)
            ->all();
    }    

    public static function getClientsLogos()
    {
        return ClientsLogo::find()->all();
    }
}

?>