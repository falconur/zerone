<?php
namespace frontend\helpers;

use yii\data\ActiveDataProvider;
use common\models\Post as Model;

class Post 
{
    public function newPosts()
    {
        return Model::find()
            ->orderBy('created_at')            
            ->all();
    }

    public function byView()
    {
        return Model::find()
            ->orderBy(['view' => SORT_DESC])            
            ->all();
    }
    
    public function mainPage()
    {
        return Model::find()          
            ->all();
    }
}