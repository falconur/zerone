var mymap = L.map('mapid').setView([41.279200, 69.242482], 13);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1Ijoibm9vcmV5MTIzIiwiYSI6ImNqcDZhbjFudTAwY2Mza28wNmVlN3Zzb3kifQ.UqRoyV2CAAvL__dmpp1WOA', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

var marker = L.marker([41.27921, 69.24235]).addTo(mymap);
