<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require dirname(dirname(__DIR__)) . '/vendor/autoload.php';
require dirname(dirname(__DIR__)) . '/vendor/yiisoft/yii2/Yii.php';
require dirname(dirname(__DIR__)) . '/common/config/bootstrap.php';
require dirname(dirname(__DIR__)) . '/backend/config/bootstrap.php';

$config = yii\helpers\ArrayHelper::merge(
    require dirname(dirname(__DIR__)) . '/common/config/main.php',
    require dirname(dirname(__DIR__)) . '/common/config/main-local.php',
    require dirname(dirname(__DIR__)) . '/backend/config/main.php',
    require dirname(dirname(__DIR__)) . '/backend/config/main-local.php'
);

(new yii\web\Application($config))->run();
