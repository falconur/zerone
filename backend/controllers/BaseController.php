<?php 

namespace backend\controllers;

use yii\web\Controller;
use Yii;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        //return var_dump(Yii::$app->user->isGuest);
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        } else 
             return parent::beforeAction($action);
    }   
}