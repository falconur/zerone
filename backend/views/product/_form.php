<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subheader_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subheader_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_uz')->widget(CKEditor::classname(), [
        'options' => ['rows' => 6]        
    ]) ?>

    <?= $form->field($model, 'description_ru')->widget(CKEditor::classname(), [
        'options' => ['rows' => 6]        
    ]) ?>
    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
