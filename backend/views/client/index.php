<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Client'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'firstname',
            'lastname',
            // 'middlename',
            'email:email',
            'phone',
            // 'region.name_'. Yii::$app->language,
            'organisation',
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDate($model->created_at);
                }
            ],           

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {send}',
                'buttons' => [
                    'send' => function ($url, $model, $key) {                    
                        return Html::a('<span class="glyphicon glyphicon-send"></span>', ['client/send', 'id' => $model->id]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
