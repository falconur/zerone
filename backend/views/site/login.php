<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-85 p-b-20">
                <span class="login100-form-title p-b-50">
                WELCOME
                </span>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <div class="wrap-input100 m-t-85 m-b-35">
                        <?= $form->field($model, 'username', [
                                'template' => '{label}{input}<span class="focus-input100" data-placeholder="Username"></span>{error}'
                        ])->textInput(['class' => 'input100', 'autofocus' => true,])->label(false) ?>
                    </div>

                    <div class="wrap-input100 m-b-50">
                        <?= $form->field($model, 'password', [
                            'template' => '{label}{input}<span class="focus-input100" data-placeholder="Password"></span>{error}'
                        ])->passwordInput(['class' => 'input100'])->label(false) ?>
                    </div>

                    <div class="container-login100-form-btn">
                        <?= Html::submitButton('Login', ['class' => 'login100-form-btn', 'name' => 'login-button']) ?>
                    </div>
                <?php ActiveForm::end(); ?>

                <ul class="login-more p-t-190">
                    <li class="m-b-8">
                        <span class="txt1">Forgot</span>
                        <?= Html::a("Username / Password?", Url::to(['site/request-password-reset']), ['class' => 'txt2']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
