<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryServices */

$this->title = Yii::t('app', 'Create Category Services');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-services-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
